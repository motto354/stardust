## using std (= < > <= >= and but-not complement implies nand nor not nxor or seq xor)
## using std/lists (cons)
## using std/testing (assert leaf tree)

## namespace std-test/bools

(defsub assert-truth-table (t f a-1 o-1
                                a-2 o-2) ## seq
  (assert t ## = (call f a-1) o-1)
  (assert t ## = (call f a-2) o-2))

(defsub assert-truth-table (t f a-1 b-1 o-1
                                a-2 b-2 o-2
                                a-3 b-3 o-3
                                a-4 b-4 o-4) ## seq
  (assert t ## = (call f a-1 b-1) o-1)
  (assert t ## = (call f a-2 b-2) o-2)
  (assert t ## = (call f a-3 b-3) o-3)
  (assert t ## = (call f a-4 b-4) o-4))

(defsub test-truth-table (name f a-1 o-1
                                 a-2 o-2)
  (leaf name ## closure (t)
    (assert-truth-table t f a-1 o-1
                            a-2 o-2)))

(defsub test-truth-table (name f a-1 b-1 o-1
                                 a-2 b-2 o-2
                                 a-3 b-3 o-3
                                 a-4 b-4 o-4)
  (leaf name ## closure (t)
    (assert-truth-table t f a-1 b-1 o-1
                            a-2 b-2 o-2
                            a-3 b-3 o-3
                            a-4 b-4 o-4)))

(defsub test-not ()
  (test-truth-table "not" not~1
    #t #f
    #f #t))

(defsub test-but-not ()
  (test-truth-table "but-not" but-not~2
    #t #t #f
    #t #f #t
    #f #t #f
    #f #f #f))

(defsub test-implies ()
  (test-truth-table "implies" implies~2
    #t #t #t
    #t #f #f
    #f #t #t
    #f #f #t))

(defsub test-and ()
  (test-truth-table "and" and~2
    #t #t #t
    #t #f #f
    #f #t #f
    #f #f #f))

(defsub test-nand ()
  (test-truth-table "nand" nand~2
    #t #t #f
    #t #f #t
    #f #t #t
    #f #f #t))

(defsub test-or ()
  (test-truth-table "or" or~2
    #t #t #t
    #t #f #t
    #f #t #t
    #f #f #f))

(defsub test-nor ()
  (test-truth-table "nor" nor~2
    #t #t #f
    #t #f #f
    #f #t #f
    #f #f #t))

(defsub test-xor ()
  (test-truth-table "xor" xor~2
    #t #t #f
    #t #f #t
    #f #t #t
    #f #f #f))

(defsub test-nxor ()
  (test-truth-table "nxor" nxor~2
    #t #t #t
    #t #f #f
    #f #t #f
    #f #f #t))

(defsub test-complement ()
  (test-truth-table "complement"
    (complement not~1)
    #t #t
    #f #f))

(defsub test-= ()
  (test-truth-table "=" =~2
    #t #t #t
    #t #f #f
    #f #t #f
    #f #f #t))

(defsub test-<=> ()
  (leaf "<=>" ## closure (t) ## seq
    (assert t ##        <  #f #t)
    (assert t ## not ## <  #t #f)
    (assert t ##        >  #t #f)
    (assert t ## not ## >  #f #t)
    (assert t ##        <= #t #t)
    (assert t ##        <= #f #t)
    (assert t ## not ## <= #t #f)
    (assert t ##        >= #t #t)
    (assert t ##        >= #t #f)
    (assert t ## not ## >= #f #t)))

(defsub test-hash ()
  (leaf "hash" ## closure (t)
    (let d {#t 'a, #f 'b} ## seq
      (assert t ## = (std/dicts/lookup! d #t) 'a)
      (assert t ## = (std/dicts/lookup! d #f) 'b))))

(defsub tree ()
  (tree "bools"
    ## cons (test-not)
    ## cons (test-but-not)
    ## cons (test-implies)
    ## cons (test-and)
    ## cons (test-nand)
    ## cons (test-or)
    ## cons (test-nor)
    ## cons (test-xor)
    ## cons (test-nxor)
    ## cons (test-complement)
    ## cons (test-=)
    ## cons (test-<=>)
    ## cons (test-hash)
    '()))
