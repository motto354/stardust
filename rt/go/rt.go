package main

import (
    σfnv "hash/fnv"
)



func SD_RT_ASSERT(name string, cond interface{}) {
    if (!cond.(SD_RT_BOOL)) {
        panic(name)
    }
}

func SD_RT_IDENTITY(value interface{}) interface{} {
    return value
}



type SD_RT_ATOM string



type SD_RT_BOOL bool



type SD_RT_TEXT string

func SD_RT_TEXT_EQUIV(x, y interface{}) bool {
    return x.(SD_RT_TEXT) == y.(SD_RT_TEXT)
}

func SD_RT_TEXT_ORDER(x, y, lt, eq, gt interface{}) interface{} {
    if x.(SD_RT_TEXT) < y.(SD_RT_TEXT) {
        return lt
    } else if x.(SD_RT_TEXT) == y.(SD_RT_TEXT) {
        return eq
    } else {
        return gt
    }
}

func SD_RT_TEXT_HASH(x interface{}) int32 {
    hash := σfnv.New32()
    hash.Write([]byte(x.(SD_RT_TEXT)))
    return int32(hash.Sum32())
}



type SD_RT_RAT struct {
    Numerator   int32
    Denominator int32
}

func SD_RT_GCD(a, b int32) int32 {
    for b != 0 {
        a, b = b, a % b
    }
    return a
}

func SD_RT_RAT_NEW(numer int32, denom int32) SD_RT_RAT {
    gcd := SD_RT_GCD(numer, denom)
    return SD_RT_RAT{
        Numerator:   numer / gcd,
        Denominator: denom / gcd,
    }
}

func SD_RT_RAT_ADD(a, b interface{}) SD_RT_RAT {
    aRat := a.(SD_RT_RAT)
    bRat := b.(SD_RT_RAT)
    return SD_RT_RAT_NEW(
        aRat.Numerator * bRat.Denominator +
            aRat.Denominator * bRat.Numerator,
        aRat.Denominator * bRat.Denominator,
    )
}

func SD_RT_RAT_MUL(a, b interface{}) SD_RT_RAT {
    aRat := a.(SD_RT_RAT)
    bRat := b.(SD_RT_RAT)
    return SD_RT_RAT_NEW(
        aRat.Numerator * bRat.Numerator,
        aRat.Denominator * bRat.Denominator,
    )
}

func SD_RT_RAT_SUB(a, b interface{}) SD_RT_RAT {
    aRat := a.(SD_RT_RAT)
    bRat := b.(SD_RT_RAT)
    return SD_RT_RAT_NEW(
        aRat.Numerator * bRat.Denominator -
            aRat.Denominator * bRat.Numerator,
        aRat.Denominator * bRat.Denominator,
    )
}



type SD_RT_LIST struct {
    Nil  bool
    Head interface{}
    Tail *SD_RT_LIST
}

func SD_RT_LIST_FOLDL(xs interface{}, z interface{}, f interface{}) interface{} {
    list := xs.(*SD_RT_LIST)
    for !list.Nil {
        z = f.(func(interface{}, interface{}) interface{})(z, list.Head)
        list = list.Tail
    }
    return z
}

func SD_RT_LIST_ZIP_WITH(xs interface{}, ys interface{}, f interface{}) *SD_RT_LIST {
    panic("Not yet implemented")
}

func SD_RT_LIST_EQUIV(xs interface{}, ys interface{}) bool {
    xsList := xs.(*SD_RT_LIST)
    ysList := ys.(*SD_RT_LIST)
    for {
        if !xsList.Nil && !ysList.Nil {
            if !Γstdφ47φ61φ126φ50(xsList.Head, ysList.Head).(SD_RT_BOOL) {
                return false
            }
            xsList = xsList.Tail
            ysList = ysList.Tail
        } else if xsList.Nil && ysList.Nil {
            return true
        } else {
            return false
        }
    }
}



// Dicts are implemented as unbalanced binary search trees. The keys used in
// the trees are the hashes of the dict keys. The values in the trees are
// arrays of collisions. Usually the arrays are singleton.
//
// TODO: Use a more efficient implementation.

type SD_RT_DICT interface {
    SD_RT_DICT_INSERT(key, value interface{}) SD_RT_DICT
    SD_RT_DICT_MEMBER(key interface{}) bool
    SD_RT_DICT_LOOKUP(key interface{}) interface{}
}

type SD_RT_DICT_ENTRY struct {
    Key   interface{}
    Value interface{}
}

type SD_RT_DICT_EMPTY struct {
}

func (SD_RT_DICT_EMPTY) SD_RT_DICT_INSERT(key, value interface{}) SD_RT_DICT {
    hash := Γstdφ47hashφ126φ49(key).(SD_RT_RAT).Numerator
    return &SD_RT_DICT_BRANCH{
        Less:    SD_RT_DICT_EMPTY{},
        Hash:    hash,
        Bucket:  []SD_RT_DICT_ENTRY{{Key: key, Value: value}},
        Greater: SD_RT_DICT_EMPTY{},
    }
}

func (SD_RT_DICT_EMPTY) SD_RT_DICT_MEMBER(key interface{}) bool {
    return false
}

func (SD_RT_DICT_EMPTY) SD_RT_DICT_LOOKUP(key interface{}) interface{} {
    return nil
}

type SD_RT_DICT_BRANCH struct {
    Less    SD_RT_DICT
    Hash    int32
    Bucket  []SD_RT_DICT_ENTRY
    Greater SD_RT_DICT
}

func (d *SD_RT_DICT_BRANCH) SD_RT_DICT_INSERT(key, value interface{}) SD_RT_DICT {
    hash := Γstdφ47hashφ126φ49(key).(SD_RT_RAT).Numerator
    if hash < d.Hash {
        less := d.Less.SD_RT_DICT_INSERT(key, value)
        return &SD_RT_DICT_BRANCH{
            Less:    less,
            Hash:    d.Hash,
            Bucket:  d.Bucket,
            Greater: d.Greater,
        }
    } else if hash == d.Hash {
        bucket := make([]SD_RT_DICT_ENTRY, len(d.Bucket) - 1)
        newEntry := SD_RT_DICT_ENTRY{Key: key, Value: value}
        found := false
        for i, entry := range d.Bucket {
            if Γstdφ47φ61φ126φ50(key, entry.Key).(SD_RT_BOOL) {
                bucket[i] = newEntry
                found = true
            } else {
                bucket[i] = entry
            }
        }
        if !found {
            bucket = append(bucket, newEntry)
        }
        return &SD_RT_DICT_BRANCH{
            Less:    d.Less,
            Hash:    d.Hash,
            Bucket:  bucket,
            Greater: d.Greater,
        }
    } else {
        greater := d.Greater.SD_RT_DICT_INSERT(key, value)
        return &SD_RT_DICT_BRANCH{
            Less:    d.Less,
            Hash:    d.Hash,
            Bucket:  d.Bucket,
            Greater: greater,
        }
    }
}

func (d *SD_RT_DICT_BRANCH) SD_RT_DICT_MEMBER(key interface{}) bool {
    hash := Γstdφ47hashφ126φ49(key).(SD_RT_RAT).Numerator
    if hash < d.Hash {
        return d.Less.SD_RT_DICT_MEMBER(key)
    } else if hash == d.Hash {
        for _, entry := range d.Bucket {
            if Γstdφ47φ61φ126φ50(key, entry.Key).(SD_RT_BOOL) {
                return true
            }
        }
        return false
    } else {
        return d.Greater.SD_RT_DICT_MEMBER(key)
    }
}

func (d *SD_RT_DICT_BRANCH) SD_RT_DICT_LOOKUP(key interface{}) interface{} {
    hash := Γstdφ47hashφ126φ49(key).(SD_RT_RAT).Numerator
    if hash < d.Hash {
        return d.Less.SD_RT_DICT_LOOKUP(key)
    } else if hash == d.Hash {
        for _, entry := range d.Bucket {
            if Γstdφ47φ61φ126φ50(key, entry.Key).(SD_RT_BOOL) {
                return entry.Value
            }
        }
        return nil
    } else {
        return d.Greater.SD_RT_DICT_LOOKUP(key)
    }
}
