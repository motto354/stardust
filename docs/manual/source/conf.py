source_suffix = ".rst"
master_doc    = "index"

html_static_path = ["_static"]

project = "Stardust"
version = "0.0.0"

html_theme = "alabaster"
html_theme_options = {
    "logo": "logo.svg",
    "show_related": True,
}
html_sidebars = {
    "**": [
        "about.html",
        "navigation.html",
        "relations.html",
        "searchbox.html",
    ],
}

highlight_language = "scheme"
