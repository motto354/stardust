type t = int ref

let start () =
  ref 0

let next t =
  let n = !t in
  let _ = t := n + 1 in
  n
