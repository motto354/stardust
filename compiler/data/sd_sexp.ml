(** S-expressions. *)

type t =
  | Atom of string
  | Bool of bool
  | Text of string
  | Rat  of int32 * int32
  | List of t list
  | Dict of (t * t) list
