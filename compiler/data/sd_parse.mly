%{
  module X = Sd_sexp

  let rec range n =
    let rec go m =
      if m >= n then []
                else m :: go (m + 1) in
    go 0
%}


%token <string> ATOM
%token <string> TEXT
%token <int>    INT

%token FALSE
%token TRUE

%token LPAREN
%token RPAREN
%token LBRACE
%token RBRACE
%token QUOTE
%token TILDE

%token HASH_HASH
%token HASH_UNDER

%token EOF

%type <Sd_sexp.t list> sexps_eof
%type <Sd_sexp.t>      sexp_eof
%type <Sd_sexp.t list> sexps
%type <Sd_sexp.t>      sexp

%start sexps_eof
%start sexp_eof
%start sexps
%start sexp

%%

sexps_eof:
  | sexps EOF           { $1 }

sexp_eof:
  | sexp EOF            { $1 }

sexps:
  |                     { [] }
  | sexp sexps          { $1 :: $2 }
  | HASH_HASH sexps     { [X.List $2] }
  | HASH_UNDER sexp sexps { $3 }

sexp:
  | ATOM                { X.Atom $1    }
  | FALSE               { X.Bool false }
  | TRUE                { X.Bool true  }
  | TEXT                { X.Text $1    }
  | INT                 { X.Rat  (Int32.of_int $1, Int32.of_int 1) }
  | LPAREN sexps RPAREN { X.List $2    }
  | LBRACE pairs RBRACE { X.Dict $2    }

  | ATOM TILDE INT      {
      let args = List.map (fun i -> X.Atom (string_of_int i)) @@ range $3 in
      let call = X.List (X.Atom $1 :: args) in
      X.List [X.Atom "closure"; X.List args; call]
    }
  | QUOTE sexp          { X.List [X.Atom "quote"; $2] }

pairs:
  |                     { [] }
  | sexp sexp pairs     { ($1, $2) :: $3 }
