{
  module P = Sd_parse

  exception End_of_input
}

let atom_head = ['a'-'z' 'A'-'Z' '/' '+' '-' '*' '%' '<' '=' '>' '.' '?' '!']
let atom_tail = (atom_head | ['0'-'9'])*

rule token = parse
  | [' ' '\n' ',']      { token lexbuf }
  | ";" ([^ '\n']*)     { token lexbuf }

  | atom_head atom_tail* as lxm
                        { P.ATOM lxm   }
  | "\""                { text_token (Buffer.create 16) lexbuf }
  | ['0'-'9']+          { P.INT (int_of_string (Lexing.lexeme lexbuf)) }

  | "#f"                { P.FALSE      }
  | "#t"                { P.TRUE       }

  | "("                 { P.LPAREN     }
  | ")"                 { P.RPAREN     }
  | "{"                 { P.LBRACE     }
  | "}"                 { P.RBRACE     }
  | "'"                 { P.QUOTE      }
  | "~"                 { P.TILDE      }

  | "##"                { P.HASH_HASH  }
  | "#_"                { P.HASH_UNDER }

  | eof                 { P.EOF        }

and text_token buf = parse
  | "\""                { P.TEXT (Buffer.contents buf) }
  | "\\0"               { Buffer.add_char buf '\x00'; text_token buf lexbuf }
  | "\\a"               { Buffer.add_char buf '\x07'; text_token buf lexbuf }
  | "\\b"               { Buffer.add_char buf '\x08'; text_token buf lexbuf }
  | "\\t"               { Buffer.add_char buf '\x09'; text_token buf lexbuf }
  | "\\n"               { Buffer.add_char buf '\x0A'; text_token buf lexbuf }
  | "\\v"               { Buffer.add_char buf '\x0B'; text_token buf lexbuf }
  | "\\f"               { Buffer.add_char buf '\x0C'; text_token buf lexbuf }
  | "\\r"               { Buffer.add_char buf '\x0D'; text_token buf lexbuf }
  | "\\\""              { Buffer.add_char buf '\x22'; text_token buf lexbuf }
  | "\\\\"              { Buffer.add_char buf '\x5C'; text_token buf lexbuf }
  | [^ '"' '\\']+       { Buffer.add_string buf (Lexing.lexeme lexbuf)
                        ; text_token buf lexbuf }
  | eof                 { raise End_of_input }
