(** Turn a program into a Go program. *)

(**
 * Thrown when a Go implementation is missing for a defforeign.
 *)
exception Missing_implementation of string

module Go : sig
  type definition
  type statement
  type expression
  type type_
  val format_definition : Sd_io.writer -> definition -> unit
  val format_statement : Sd_io.writer -> statement -> unit
  val format_expression : Sd_io.writer -> int -> expression -> unit
  val format_type : Sd_io.writer -> type_ -> unit
end

val gen_translation_unit
  :  Sd_name.qglobal Sd_syntax.Make (Sd_anf).translation_unit
  -> Go.definition list

val gen_definition
  :  string list
  -> Sd_name.qglobal Sd_syntax.Make (Sd_anf).definition
  -> Go.definition list

val gen_contract
  :  Sd_name.qglobal Sd_syntax.Make (Sd_anf).contract
  -> Go.statement list
  -> Go.statement list

val gen_expression
  :  string
  -> Sd_name.qglobal Sd_anf.expression
  -> Go.statement list

val gen_value
  :  Sd_name.qglobal Sd_anf.value
  -> Go.expression
