(** Generate HTML from documentation. *)

(* TODO: For some reason using Sd_syntax.Calculus here instead of a sig literal
 * results in an error. Investigate why this happens. *)
module Make (Calc : sig type 'g expression end) : sig
  val gen_translation_unit
    :  Sd_io.writer
    -> 'q Sd_syntax.Make (Calc).translation_unit
    -> unit

  val gen_definition
    :  Sd_io.writer
    -> 'q Sd_syntax.Make (Calc).definition
    -> unit
end
