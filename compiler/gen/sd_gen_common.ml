let parens w s p f =
  begin
    begin if s < p then Sd_io.write_all w "(" end;
    let r = f () in
    begin if s < p then Sd_io.write_all w ")" end;
    r
  end
