(** Given self and parent precedences, apply parentheses if necessary. *)
val parens : Sd_io.writer -> int -> int -> (unit -> 'a) -> 'a
