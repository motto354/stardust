(**
 * Lambda calculus is a simple yet Turing-complete formalism for expressing
 * computation. Lambda calculus is the first internal representation in the
 * compilation pipeline, converted into from S-expressions by the recognition
 * phase and converted into administrative normal form by the sequentialization
 * phase.
 *)

(**
 * Expressions occur within subroutine definitions and other expressions. They
 * are evaluated to produce values and side-effects.
 *)
type 'g expression =
  | Using_expression of string list * string list * 'g expression
  | Variable of string
  | Literal of Sd_literal.t
  | Apply of 'g * 'g expression list
  | Call of 'g expression * 'g expression list
  | Closure of string list * 'g expression
  | If of 'g expression * 'g expression * 'g expression
  | Out
