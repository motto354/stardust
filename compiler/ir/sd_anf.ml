(**
 * Administrative normal form is a variant of lambda calculus that imposes
 * additional restrictions on how expressions can be nested. These restrictions
 * make it easier to generate code in imperative target languages.
 *
 * The restrictions are such that there are two classes of terms: values and
 * expressions. Values always immediately evaluate to a value, by either being
 * a constant, a lambda, or a variable. Expressions allow conditionals and
 * subroutine calls, but the conditions and arguments may only be values. Let
 * bindings are supported in expressions to accomodate the latter.
 *
 * ANF is generated from lambda calculus by the sequentialization phase.
 *)

(**
 * Expressions occur within subroutine definitions and let expressions. They
 * are evaluated to produce values and side-effects.
 *)
type 'g expression =
  | Value of 'g value
  | Let of string * 'g expression * 'g expression
  | Apply of 'g * 'g value list
  | Call of 'g value * 'g value list
  | If of 'g value * 'g expression * 'g expression

(**
 * Unlike expressions, values may occur as subroutine arguments.
 *)
and 'g value =
  | Variable of string
  | Literal of Sd_literal.t
  | Closure of string list * 'g expression
  | Out
