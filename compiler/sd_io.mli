(** I/O streams. *)

type writer = <
  write : bytes -> int -> int -> unit
>

val write_all : writer -> bytes -> unit

val writer_of_out_channel : out_channel -> writer
