module A = Sd_anf
module L = Sd_lambda

module Sa = Sd_syntax.Make (A)
module Sl = Sd_syntax.Make (L)

module Supply = Sd_supply

(** Generate a variable name for a temporary. *)
let temporary (sup : Supply.t) : string =
  "SD_TMP_" ^ string_of_int (Supply.next sup)

(**
 * Bind an expression and make it available as a value to the continuation. The
 * continuation is wrapped inside a let binding.
 *
 * If the expression is already a value, do not bother binding it.
 *)
let bind (sup : Supply.t)
         (expr : 'g A.expression)
         (cont : 'g A.value -> 'a) : 'a =
  match expr with
  | A.Value value -> cont value
  | _ -> let tmp = temporary sup in
         A.Let (tmp, expr, cont (A.Variable tmp))

(** Like {!val:bind}, but bind multiple expressions. *)
let rec binds (sup : Supply.t)
              (exprs : 'g A.expression list)
              (cont : 'g A.value list -> 'a) : 'a =
  match exprs with
  | [] -> cont []
  | hd :: tl -> bind sup hd @@ fun hd' ->
                  binds sup tl (fun tl' -> cont (hd' :: tl'))

let rec sequentialize_translation_unit sup tu =
  List.map (sequentialize_definition sup) tu

and sequentialize_definition sup = function
  | Sl.Using_definition (namespace, names, defs) ->
      let defs' = List.map (sequentialize_definition sup) defs in
      Sa.Using_definition (namespace, names, defs')

  | Sl.Namespace (name, defs) ->
      let defs' = List.map (sequentialize_definition sup) defs in
      Sa.Namespace (name, defs')

  | Sl.Defsub (name, params, contract, body) ->
      let contract' = sequentialize_contract sup contract in
      let body'     = sequentialize_expression sup body in
      Sa.Defsub (name, params, contract', body')

  | Sl.Defforeign (name, params, contract, impls) ->
      let contract' = sequentialize_contract sup contract in
      Sa.Defforeign (name, params, contract', impls)

  | Sl.Defmeth (name, params) ->
      Sa.Defmeth (name, params)

  | Sl.Defimpl (meth, cls, params, body) ->
      let body' = sequentialize_expression sup body in
      Sa.Defimpl (meth, cls, params, body')

  | Sl.Docarticle (title, body) ->
      Sa.Docarticle (title, body)

and sequentialize_contract sup contract =
  let sequentialize_cond (name, cond) =
        (name, sequentialize_expression sup cond) in
  { Sa.requires = List.map sequentialize_cond contract.Sl.requires
  ; Sa.ensures  = List.map sequentialize_cond contract.Sl.ensures  }

and sequentialize_expression sup = function
  | L.Using_expression (_, _, expr) ->
      sequentialize_expression sup expr

  | L.Variable name ->
      A.Value (A.Variable name)

  | L.Literal literal ->
      A.Value (A.Literal literal)

  | L.Apply (callee, args) ->
      let args' = List.map (sequentialize_expression sup) args in

      binds sup args' @@ fun args'' ->
        A.Apply (callee, args'')

  | L.Call (callee, args) ->
      let callee' = sequentialize_expression sup callee in
      let args'   = List.map (sequentialize_expression sup) args in

      bind sup callee' @@ fun callee'' ->
      binds sup args'  @@ fun args'' ->
        begin
          match (callee'', args'') with
          | (A.Closure ([param], body), [arg]) ->
              A.Let (param, A.Value arg, body)
          | _ ->
              A.Call (callee'', args'')
        end

  | L.Closure (params, body) ->
      let body' = sequentialize_expression sup body in
      A.Value (A.Closure (params, body'))

  | L.If (cond, if_true, if_false) ->
      let cond'     = sequentialize_expression sup cond in
      let if_true'  = sequentialize_expression sup if_true in
      let if_false' = sequentialize_expression sup if_false in

      bind sup cond' @@ fun cond'' ->
        A.If (cond'', if_true', if_false')

  | L.Out ->
      A.Value A.Out
